from nltk.book import *

# concordancia de palabras en un texto

text5.concordance("great")
text7.concordance("arms")
text1.concordance("terrible")

# similitud entre palabras com similar

text1.similar("terrible")
text2.similar("terrible")
text5.similar("terrible")
text6.similar("terrible")
