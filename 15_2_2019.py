import nltk
import matplotlib
from nltk.corpus import brown

cfreqdist = nltk.ConditionalFreqDist((genre, word) for genre in brown.categories() for word in brown.words(categories=genre))
genres = ['romance', 'news']
days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
cfreqdist.tabulate(conditions=genres, samples=days)
cfreqdist.plot(conditions=genres, samples=days)
