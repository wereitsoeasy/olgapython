from functools import reduce
from bs4 import BeautifulSoup
import nltk
from nltk.corpus import PlaintextCorpusReader

root = '/home/seven/Downloads/'

lists = PlaintextCorpusReader(root, '.*')

# print('100 primeras words')
# print(lists.words('e990101.htm')[0:100])

# print('100 primeros sents')
# print(lists.sents('e990101.htm')[0:5])

# print('concordance')
f = open('/home/seven/Downloads/e990101.htm', 'r')
text = f.read()
text = text.lower()
# tokens = nltk.Text(nltk.word_tokenize(text))
# tokens.concordance('servicio')

# tokens.similar('claro')
# print(list(tokens))

soup = BeautifulSoup(text, 'lxml')
text = soup.get_text()
tokens = nltk.Text(nltk.word_tokenize(text))

print(list(filter(lambda stri: stri.isalpha(), list(tokens)))[0:100])
