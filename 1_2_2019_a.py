#!/usr/bin/python
# -*- coding: utf-8 -*-
# pasitos
# leer el archivo
# escribir la oracion en otro archivo
# cambiar primera letra de cada linea por un gato
# si la primera letra era vocal o consonante, ponerle un $VOC o $CONS al final
f = open('1_2_2019_a.txt', 'r')
f2 = open('1_2_2019_a_copia.txt', 'w')
lineaux = ''
fileaux = []
signitos = ['\"', '.', '!', ',', '\'']
for line in f:
    lineaux = line.split()
    for word in lineaux:
        for schar in signitos:
            if schar in word:
                word = word.strip('\".!,\'')
        if word[0] == 'a' or  word[0] == 'e' or word[0] == 'i' or word[0] == 'o' or word[0] == 'u':
            word+='$VOC'
        else:
            word+='$CONS'
        word = '#' + word[1:]
        fileaux.append(word)
    f2.write(' '.join(fileaux))

print(fileaux)
print("--al revés")
print(fileaux[::-1])
