f = open('1_2_2019_b.txt')

ptext = str(f.read()).split()
plist = []
numeros = ['0','1','2','3','4','5','6','7','8','9']
nlist = []

for i in range(0,len(ptext)):
    if not any(n in numeros for n in ptext[i]):
        nlist.append(ptext[i])

xlist = list(map(lambda s: s.strip('¬¿?\/^()«»´<>.,-_+*~[]{}=°;:\'\"#$%&()!|0123456789'), nlist))

uset = set(xlist)

print(xlist)
print("hay como %d palabras" % len(xlist))
print("son como %d palabras únicas" % len(uset))

