#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#codigo para limpiar el generate.txt
def procesarlineas(texto):
    for lineas in texto:
        if(list(lineas.split())) !=[]:
            f2.write(list(lineas.split())[0].replace("#","")+" "+ list(lineas.split())[-1]+"\n")

f=open("generate.txt","r",encoding="latin-1")
texto=f.readlines()
f.close()
f2=open("generate_l.txt","w")
procesarlineas(texto)
f.close()
f2.close()
### Codigo para obtener lemas en un archivo distinto
f=open("generate_l.txt","r", encoding="latin-1")
texto=f.readlines()
f.close()
diccionario={}
for lineas in texto:
    diccionario[lineas.split()[0]]=lineas.split()[1]
f2=open("freqlist.txt","r",encoding="latin-1")
palabras=f2.readlines()
f2.close()
f3=open("lemas.txt","w",encoding="latin-1")
for lineas in palabras:
    if(lineas.split()[0]) in diccionario.keys():
        f3.write(diccionario[lineas.split()[0]]+ "\n")
    else:
        f3.write(lineas.split()[0]+ "\n")
f3.close()
