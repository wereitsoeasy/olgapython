#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import operator

def getIndice(vocabulario,palabra):
    salida=-1
    for i in range(0,len(vocabulario)):
        if(palabra==vocabulario[i]):
            salida=i
            i=len(vocabulario)+2
    return salida  

def similitud(indice,matriz,vocabulario):
    probabilidad={}
    vab=matriz[indice]
    for i in range(0,len(vocabulario)):
        punto=0
        prob=0
        if(indice!=i):
            for j in range(0,len(matriz[i])):
                punto+=vab[j]*matriz[i][j]
            prob=punto/(len(matriz[i])*len(vab))
        probabilidad[vocabulario[i]]=prob
    return probabilidad


lem=[]
texto=(line.strip() for line in open('lemas.txt'))
for items in texto:
    lem.append(items)
vocabulario=sorted(set(lem))

lista_contex=[]
sim={}

palabra_busq="priístas"
for palabra in vocabulario:
    if palabra:
        palabra_c=[]
        for indice in range(0,len(lem)):
            if lem[indice]:
                if (palabra==lem[indice]):
                    if(indice<4):
                        for i in range(0,indice):
                            palabra_c.append(lem[i])
                        for i in range(indice+1,indice+5):
                            palabra_c.append(lem[i])
                    elif((len(lem)-indice)<5):
                        for i in range(indice+1,len(lem)):
                            palabra_c.append(lem[i])
                        for i in range(indice-4,indice):
                            palabra_c.append(lem[i])
                    else:
                        for i in range(indice+1,indice+5):
                            palabra_c.append(lem[i])
                        for i in range(indice-4,indice):
                            palabra_c.append(lem[i])
        lista_contex.append(palabra_c) 
M_simil=[] #matirz similitud
for pos_vocabulario in range(0,len(vocabulario)):
    vector=[]
    for palabra in vocabulario:
        if lista_contex[pos_vocabulario]:
            if(palabra in lista_contex[pos_vocabulario]):
                vector.append(lista_contex[pos_vocabulario].count(palabra))
            else:
                vector.append(0)
    M_simil.append(vector)

indice_a_buscar=getIndice(vocabulario,palabra_busq)
if(indice_a_buscar!=-1):
    sim=similitud(indice_a_buscar,M_simil,vocabulario)
else:
    print("intenta de nuevo") 

resultado = sorted(sim.items(),key=operator.itemgetter(1),reverse=True)

sim_f={}
contador=0

for elemento in resultado:
    if(elemento[1]!=0.0):
        if contador<10:
            sim_f[elemento[0]]=elemento[1]
            contador+=1
        else:
            break;
print("similitud de palabra",palabra_busq,"\n")
for elemento in sim_f:
    print(elemento + " : " + str(sim_f[elemento]))
